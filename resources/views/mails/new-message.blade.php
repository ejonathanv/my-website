@component('mail::message')
# Nuevo mensaje desde sitio web.

Se ha recibido un nuevo mensaje de: <br>
Nombre: {{ $name }} <br>
Correo electrónico: {{ $email }} <br>
Descripción: {{ $description }} <br>

{{ config('app.name') }}
@endcomponent
