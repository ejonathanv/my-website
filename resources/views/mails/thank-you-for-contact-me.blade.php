@component('mail::message')
# ¡Gracias por escribirme!

Hola {{ $name }}, te escribe Jonathan Velazquez! <br>
Gracias por comunicarte conmigo, estoy revisando tu mensaje y en la brevedad posible me comunico contigo para darle seguimiento. <br>

Gracias,<br>
{{ config('app.name') }}
@endcomponent
