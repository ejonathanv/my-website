<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'link' => 'nullable|string'
        ];
    }

    public function messages(){
        return [
            'title.required' => 'El título es requerido.',
            'title.string' => 'El título no es válido.',
            'description.required' => 'La descripción es requerida.',
            'description.string' => 'La descripción no es válida.',
            'link.string' => 'El enlace no es válido.',
        ];
    }
}
