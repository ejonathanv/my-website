<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|unique:articles',
            'description' => 'required|string',
            'body' => 'required|string',
            'date' => 'required|date'
        ];
    }

    public function messages(){
        return [
            'title.required' => 'El título es requerido.',
            'title.string' => 'El título no es válido.',
            'title.unique' => 'Ya existe un artículo con el mismo título.',
            'description.required' => 'La descripción es requerida.',
            'description.string' => 'La descripción no es válida.',
            'body.required' => 'Por favor agrega el contenido del artículo.',
            'body.string' => 'El contenido no es válido.',
            'date.required' => 'La fecha del artículo es requerida.',
            'date.date' => 'La fecha no es válida.'
        ];
    }
}
