<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'nullable|min:6|max:15'
        ];
    }

    public function messages(){
        return [

            'name.required' => 'Por favor ingresa tu nombre completo',
            'name.string' => 'El nombre no es válido',
            'email.required' => 'Por favor ingresa tu correo electrónico',
            'email.email' => 'El correo electrónico proporcionado no es válido',
            'password.min' => 'La contraseña debe tener al menos 6 caracteres',
            'password.max' => 'La contraseña debe tener máximo 15 caracteres'

        ];
    }
}
