<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectCoverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cover' => 'required|image|max:2000'
        ];
    }


    public function messages(){
        return [

            'cover.required' => 'Por favor selecciona una imagen.',
            'cover.image' => 'El archivo seleccionado no es una imagen.',
            'cover.max' => 'La imagen seleccionada debe optimizarse a menos de 2MB',

        ];
    }
}
