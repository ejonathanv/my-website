<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PersonalInfoRequest;
use Inertia\Inertia;

class ConfigController extends Controller
{
    public function index(){
        return Inertia::render('Admin/Config/Index');
    }

    public function store(PersonalInfoRequest $request){
        $user = auth()->user();
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password){
            info('Vamos a cambiar el password');
            $user->password = \Hash::make($request->password);
        }
        $user->save();
        return redirect()->back()->with('personalinfo', 'La información del usuario ha sido actualizada.');
    }
}
