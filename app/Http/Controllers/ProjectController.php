<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Http\Requests\ProjectCoverRequest;

use Inertia\Inertia;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::orderBy('created_at', 'desc')->get()->map(function($project){
            $project->cover = asset($project->cover);
            return $project;
        });
        return Inertia::render('Admin/Projects/Index', ['projects' => $projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Admin/Projects/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProjectRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProjectRequest $request)
    {
        $project = $this->setProjectValues($request, new Project());
        return redirect()->route('projects.edit', $project)->with('projectinfo', 'Se ha guardado el proyecto exitosamente.');
    }

    public function setProjectValues($request, $project){
        $project->title = $request->title;
        $project->description = $request->description;
        $project->link = $request->link;
        $project->save();

        return $project;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {

        $project->cover = asset($project->cover);

        return Inertia::render('Admin/Projects/Edit', [
            'project' => $project
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProjectRequest  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        $this->setProjectValues($request, $project);
        return redirect()->back()->with('projectinfo', 'El proyecto ha sido actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->route('projects.index')->with('status', 'El proyecto ha sido eliminado.');
    }

    public function cover(ProjectCoverRequest $request, Project $project)
    {
        $cover = $request->file('cover')->store('uploads', 'uploads');
        $project->cover = $cover;
        $project->save();

        return redirect()
            ->route('projects.edit', $project->id)
            ->with('projectcover', 'Se ha actualizado la portada del proyecto.');
        
    }

    public function visibility(Request $request, Project $project){
        $project->visibility = $request->visibility;
        $project->save();

        if($project->visibility){
            $response = 'El proyecto ha sido publicado.';
        }else{
            $response = 'El proyecto ahora es privado.';
        }
        return redirect()->back()->with('ProjectVisibilityStatus', $response);
    }
}
