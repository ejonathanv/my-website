<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Inertia\Inertia;

use App\Models\Subscription;
use App\Models\Message;
use App\Mail\NewMessage;
use App\Mail\ThankYouForContactMe;

use App\Models\Article;

use Carbon\Carbon;

use Mail;

class WebsiteController extends Controller
{
    public function index(){
        $projects = \DB::table('projects')
            ->where('visibility', 1)
            ->select(['id', 'title', 'description', 'link', 'cover'])
            ->get()
            ->map(function($project){
                $project->cover = asset($project->cover);
                return $project;
            });

        return Inertia::render('Website/Index', ['projects' => $projects]);
    }

    public function about(){
        return Inertia::render('Website/About');
    }

    public function contact(){
        return Inertia::render('Website/Contact');
    }

    public function blog(){
        $articles = \DB::table('articles')
            ->where('visibility', 1)
            ->select(['cover', 'title', 'description', 'body', 'date', 'slug'])
            ->get()
            ->map(function($article){
                $article->date = Carbon::parse($article->date)->format('d M, Y');
                $article->cover = asset($article->cover);
                return $article;
            });
        
        return Inertia::render('Website/Blog', ['articles' => $articles]);
    }

    public function article(Article $article){

        if(!$article->visibility){
            return abort(404);
        }

        $article->date = Carbon::parse($article->date)->format('d M, Y');
        $article->cover = asset($article->cover);
        return Inertia::render('Website/Article', ['article' => $article]);
    }

    public function contactForm(Request $request){

        $rules = $this->contactValidationRules($request);
        $messages = $this->contactValidationMessages($request);
        $request->validate($rules, $messages);

        $this->sendEmailToAdmin($request);
        $this->sendEmailToClient($request);
        $this->userSubscription($request);
        $this->storeMessage($request);

        return redirect()
            ->back()
            ->with('status', "{$request->full_name}, ¡Gracias por escribirme! me comunicaré contigo lo más pronto posible.");
        
    }

    public function storeMessage($request){
        $message = new Message();
        $message->full_name = $request->full_name;
        $message->email = $request->email;
        $message->type_id = $request->type_id;
        $message->time_id = $request->time_id;
        $message->description = $request->description;
        $message->save();

        return $message;
    }

    public function userSubscription($request){
        if(!$request->subscription){
            return false;
        }

        if(Subscription::where('email', $request->email)->first()){
            return false;
        }

        $subscription = new Subscription();
        $subscription->name = $request->full_name;
        $subscription->email = $request->email;
        $subscription->save();

        return true;
    }

    public function sendEmailToAdmin($request){
        Mail::to(env('MAIL_FOR_CONTACT'))->send(new NewMessage($request));
    }

    public function sendEmailToClient($request){
        Mail::to($request->email)->send(new ThankYouForContactMe($request));
    }

    public function contactValidationRules(){
        $rules = [

            'full_name' => 'required|string',
            'email' => 'required|email',
            'type_id' => 'required|in:1,2,3,4,5,6,7',
            'time_id' => 'required|in:1,2,3,4,5,6',
            'description' => 'required|string',
            'terms' => 'required|accepted',
            'subscription' => 'sometimes|boolean',

        ];

        return $rules;
    }

    public function contactValidationMessages(){
        $messages = [
            'full_name.required' => 'Por favor ingresa tu nombre completo.',
            'full_name.string' => 'El nombre no es válido.',
            'email.required' => 'Por favor ingresa tu correo electrónico.',
            'email.email' => 'El correo electrónico proporcionado no es válido.',
            'type_id.required' => 'El tipo de proyecto es requerido',
            'type_id.in' => 'El tipo de proyecto seleccionado no es válido', 
            'time_id.required' => 'Define en cuanto tiempo necesitas tu proyecto', 
            'time_id.in' => 'Selecciona una opción válida', 
            'description.required' => 'La descripción es requerida.',
            'description.string' => 'La descripción no es válida.',
            'terms.required' => 'Por favor lee y acepta la Política de privacidad.',
            'terms.accepted' => 'Por favor lee y acepta la Política de privacidad.',
            'subscription.sometimes' => 'Ha ocurrido un error, por favor intenta nuevamente.',
            'subscription.boolean' => 'Ha ocurrido un error, por favor intenta nuevamente.'
        ];

        return $messages;
    }

    public function terms(){
        return Inertia::render('Website/Terms');
    }

}
