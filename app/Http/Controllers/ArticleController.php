<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Http\Requests\ArticleCoverRequest;

use Inertia\Inertia;
use Carbon\Carbon;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('date')->get()->map(function($article){
            $article->cover = asset($article->cover);
            return $article;
        });
        return Inertia::render('Admin/Articles/Index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date = Carbon::now()->format('Y-m-d');
        return Inertia::render('Admin/Articles/Create', ['date' => $date]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreArticleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticleRequest $request)
    {
        $article = $this->setArticleValues($request, new Article());
        return redirect()->route('articles.edit', $article)->with('articleinfo', 'Se ha guardado el artículo exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $article->date = Carbon::parse($article->date)->format('Y-m-d');
        $article->cover = asset($article->cover);

        return Inertia::render('Admin/Articles/Edit', ['article' => $article]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateArticleRequest  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArticleRequest $request, Article $article)
    {
        $this->setArticleValues($request, $article);
        return redirect()->back()->with('articleinfo', 'El artículo ha sido actualizado.');
    }

    public function setArticleValues($request, $article){
        $article->date = Carbon::parse($request->date);
        $article->title = $request->title;
        $article->description = $request->description;
        $article->body = $request->body;
        $article->slug = \Str::slug($request->title);
        $article->save();

        return $article;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->route('articles.index')->with('status', 'El artículo ha sido eliminado.');
    }

    public function cover(ArticleCoverRequest $request, Article $article)
    {
        $cover = $request->file('cover')->store('uploads', 'uploads');
        $article->cover = $cover;
        $article->save();

        return redirect()->route('articles.edit', $article->id)->with('articlecover', 'Se ha actualizado la portada del artículo.');
        
    }

    public function visibility(Request $request, Article $article){
        $article->visibility = $request->visibility;
        $article->save();

        if($article->visibility){
            $response = 'El artículo ha sido publicado.';
        }else{
            $response = 'El artículo ahora es privado.';
        }
        return redirect()->back()->with('visibilityStatus', $response);
    }

}
