<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Article;
use App\Models\Project;
use App\Models\Message;

class DashboardController extends Controller
{
    public function dashboard(){

        $indicators = [
            'articles' => Article::count(),
            'projects' => Project::count(),
            'messages' => 34,
            'suscriptors' => 12
        ];

        $messages = Message::orderBy('created_at', 'desc')->take(10)->get();

        return Inertia::render('Admin/Dashboard', [
            'indicators' => $indicators,
            'messages' => $messages
        ]);
    }
}
