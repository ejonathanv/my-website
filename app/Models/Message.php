<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $appends = ['type', 'time', 'date'];

    public function getTypeAttribute(){
        switch ($this->type_id) {
            case 1:
                return 'Una pregunta en general';
                break;
            
            case 2:
                return 'Necesito una página web';
                break;

            case 3:
                return 'Necesito ayuda con mi sitio actual';
                break;

            case 4:
                return 'Necesito un rediseño web';
                break;

            case 5:
                return 'Diseño Gráfico / Imágen corporativa';
                break;

            case 6:
                return 'Fotografía';
                break;

            case 7:
                return 'Solo un saludo';
                break;

            default: 
                return 'Sin definir';
                break;

        }
    }

    public function getTimeAttribute(){
        switch ($this->time_id) {
            case 1:
                return 'Con mucha urgencia';
                break;

            case 2:
                return 'No llevo tanta prisa';
                break;

            default: 
                return 'Sin definir';
                break;
        
        }
    }

    public function getDateAttribute(){
        return $this->created_at->format('d M, Y - h:i a');
    }
}
