<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ThankYouForContactMe extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->request->full_name;

        return $this->subject("{$name}, ¡Gracias por escribirme!")
            ->markdown('mails.thank-you-for-contact-me')
            ->with([
                'name' => $this->request->full_name,
                'email' => $this->request->email
            ]);
    }
}
