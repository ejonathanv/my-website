<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->realText(50),
            'description' => $this->faker->realText(120),
            'cover' => 'https://picsum.photos/1100/600',
            'link' => 'http://localhost:8000',
        ];
    }
}
