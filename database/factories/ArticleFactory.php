<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use Carbon\Carbon;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->realText(60);
        $date = Carbon::now()->subDays(rand(0,10));

        return [
            'cover' => 'https://picsum.photos/820/550',
            'title' => $title,
            'description' => $this->faker->realText(120),
            'body' => $this->faker->randomHtml(2,3),
            'date' => $date,
            'slug' => \Str::slug($title)
        ];
    }
}
