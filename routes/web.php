<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebsiteController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\ConfigController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WebsiteController::class, 'index'])->name('home');
Route::get('/acerca-de-mi', [WebsiteController::class, 'about'])->name('about');
Route::get('/contactame', [WebsiteController::class, 'contact'])->name('contact');
Route::get('/blog', [WebsiteController::class, 'blog'])->name('blog');
Route::get('/blog/{article:slug}', [WebsiteController::class, 'article'])->name('article');
Route::post('/contact', [WebsiteController::class, 'contactForm'])->name('contact-form');
Route::get('/terminos-y-condiciones', [WebsiteController::class, 'terms'])->name('terms');


Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'verified']], function(){

    Route::get('/', [DashboardController::class, 'dashboard'])->name('dashboard');
    Route::resource('/articles', ArticleController::class);
    Route::post('/articles/{article}/cover', [ArticleController::class, 'cover'])->name('articles.cover');
    Route::put('/articles/{article}/visibility', [ArticleController::class, 'visibility'])->name('articles.toggle-visibility');
    Route::resource('/projects', ProjectController::class);
    Route::post('/projects/{project}/cover', [ProjectController::class, 'cover'])->name('projects.cover');
    Route::put('/projects/{project}/visibility', [ProjectController::class, 'visibility'])->name('projects.toggle-visibility');
    Route::resource('/subscriptors', SubscriptionController::class);
    Route::get('/configuration', [ConfigController::class, 'index'])->name('configuration');
    Route::post('/configuration', [ConfigController::class, 'store'])->name('save-configuration');

});

require __DIR__.'/auth.php';
